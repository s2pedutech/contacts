var express = require('express');
var app = express();
var cors = require('cors');
var bodyp = require('body-parser');
var routes = require('./routes/routes');
app.use(cors());
app.use(bodyp.json());

app.listen(3000, () => {
    console.log("Listening on 3000");
});
app.use('/',routes);
