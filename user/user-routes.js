var app = require('express').Router();
var userRoutes = require('./user');
app.get('/',userRoutes.getAll);
app.post('/signup', userRoutes.signUp);
app.post('/login', userRoutes.login);
app.put('/update', userRoutes.update);
app.delete('/delete/:id', userRoutes.delete);
module.exports = app;