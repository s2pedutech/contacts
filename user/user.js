var mysql = require('../db/db');
var user = {
    signUp: (req,res) => {
        let userData = req.body;
        let q = "insert into user (name, email, password, mobile, role) ";
        q += "values (?,?,?,?,?)";

        mysql.query(q, [userData.name, userData.email,userData.password, userData.mobile, userData.role], (err,rows) => {
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
    },
    login: (req,res) => {
        let userData = req.body;
        let q = "select * from user where email = ? and password = ?";
        mysql.query(q,[userData.email, userData.password], (err,rows) => {
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
    },
    getAll: (req,res) => {
        let q = "select * from user where role=?";
        mysql.query(q,['user'], (err,rows) => {
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
    },
    update: (req,res) => {},
    delete: (req,res) => {}
};

module.exports = user;