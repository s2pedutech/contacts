var app = require('express').Router();
var userRoutes = require('../user/user-routes');
var contactRoutes = require('../contacts/contacts-routes');
app.use('/user', userRoutes);
app.use('/contact', contactRoutes);
module.exports = app;