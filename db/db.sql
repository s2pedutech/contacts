drop database if exists contactsdb;

create database contactsdb;
use contactsdb;

drop table if exists user;
create table user(
    id int primary key auto_increment,
    name varchar(100) not null,
    email varchar(100) not null unique,
    password varchar(20) not null unique,
    mobile varchar(20) not null unique,
    role varchar(20) not null 
);

drop table if exists contacts;
create table contacts(
    id int primary key auto_increment,
    name varchar(100) not null,
    email varchar(100) not null,
    mobile varchar(20) not null,
    userId int not null,
    city varchar(100) not null,
    state varchar(100) not null
);