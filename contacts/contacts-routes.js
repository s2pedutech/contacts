var app = require('express').Router();
var contact = require('./contacts');
app.post('/add', contact.add);
app.put('/update', contact.update);
app.delete('/delete/:id', contact.delete);
app.get('/getById/:id', contact.getById);
app.get('/getByUserId/:id', contact.getAllByUserId);
app.get('/', contact.getAll);
module.exports = app;