var mysql = require('../db/db');
var contacts = {
    getAll : (req,res) => {
        let q = "select * from contacts";
        mysql.query(q, (err,rows) => {
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
    },
    add : (req,res) => {
        let contact = req.body;
        let q = "insert into contacts (userId,name,email,mobile,city,state) values (?,?,?,?,?,?)";
        mysql.query(q,[contact.userId,contact.name, contact.email, contact.mobile,contact.city, contact.state], (err,rows) => {
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
    },
    getAllByUserId : (req,res) => {
        let id = req.params.id;
        let q = "select * from contacts where userId=?";
        mysql.query(q,[id], (err,rows) => {
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
    },
    getById : (req,res) => {
        let id = req.params.id;
        let q = "select * from contacts where id=?";
        mysql.query(q,[id], (err,rows) => {
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
    },
    update : (req,res) => {
        let cont = req.body;
        let q = "update contacts set name=?,email=?,mobile=?,city=?, state=? where id=?";
        mysql.query(q,[cont.name, cont.email,cont.mobile,cont.city, cont.state, cont.id], (err,rows) => {
            if(err)
                res.send(err);
            else
                res.send(rows);
        });
    },
    delete: (req,res) => {
        let id = req.params.id;
        let q = "delete from contacts where id=?";
        mysql.query(q,[id], (err,rows) => {
            if(err)
                res.send(err);
            else
                res.send(rows);
        }); 
    }
};

module.exports = contacts;